<?php

require 'autoload.php';

$Framework = new \app\Framework();

// Контроллер отрисовки фигур
$Framework->post('/draw-shapes', function (\app\Request $Request, \app\Response $Response) {

    $shapes = $Request->getFormAsArray('shapes');
    $result = '';
    foreach ($shapes as $shapeConfig) {
        $Shape = \app\ShapeFactory::getShape($shapeConfig);
        $result .= $Shape->drawAsImage() . '<br>';
        $result .= $Shape->drawAsArrayOfPoints() . '<br>';
        $result .= $Shape->drawAsSomethingElse() . '<br>';
    }
    $Response->write($result);

    return $Response;
});

$Framework->run();