<?php

namespace app;

/**
 * Class Circle
 * @package app
 */
class Circle extends Shape
{
    /**
     * @inheritdoc
     */
    public function drawAsImage()
    {
        // реализация отрисовки как изображения
    }
    /**
     * @inheritdoc
     */
    public function drawAsArrayOfPoints()
    {
        // реализация отрисовки как массив точек
    }
    /**
     * @inheritdoc
     */
    public function drawAsSomethingElse()
    {
        // реализация отрисовки как что-то еще
    }
}