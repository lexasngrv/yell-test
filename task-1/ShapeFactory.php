<?php

namespace app;

/**
 * Фабрика фигур
 * @package app
 */
class ShapeFactory
{
    /**
     * Получение экземпляра фигуры
     * @param array $shapeConfig
     * @return Shape
     * @throws \Exception
     */
    public static function getShape(array $shapeConfig)
    {
        if (empty($shapeConfig['type']) || empty($shapeConfig['params'])) {
            throw new \Exception('Shape config is incorrect!');
        }

        switch ($shapeConfig['type']) {
            case 'square':
                return new Square($shapeConfig['params']);
            case 'circle':
                return new Circle($shapeConfig['params']);
            default:
                throw new \Exception('Type isn\'t supported!');
        }
    }
}