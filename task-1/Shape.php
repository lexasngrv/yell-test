<?php

namespace app;

/**
 * Базовый класс для всех фигур
 */
abstract class Shape
{
    /**
     * @var string цвет
     */
    public $color;
    /**
     * @var integer ширина линии
     */
    public $width;
    /**
     * @var string размер
     */
    public $size;

    /**
     * Конструктор
     * @param array $shapeParams
     * @throws \Exception
     */
    public function __construct($shapeParams)
    {
        if (empty($shapeParams['color']) || empty($shapeParams['width']) || empty($shapeParams['size'])) {
            throw new \Exception('Shape params are incorrect!');
        }

        $this->color = $shapeParams['color'];
        $this->width = $shapeParams['width'];
        $this->size = $shapeParams['size'];
    }

    /**
     * Отрисовка как изображение
     * @return mixed
     */
    abstract public function drawAsImage();
    /**
     * Отрисовка как массив точек
     * @return mixed
     */
    abstract public function drawAsArrayOfPoints();
    /**
     * Отрисовка как что-то еще
     * @return mixed
     */
    abstract public function drawAsSomethingElse();
}