CREATE TABLE authors (
  id BIGINT PRIMARY KEY NOT NULL DEFAULT nextval('authors_id_seq'::regclass),
  name CHARACTER VARYING(255)
);
CREATE TABLE books (
  id BIGINT PRIMARY KEY NOT NULL DEFAULT nextval('books_id_seq'::regclass),
  name CHARACTER VARYING(255)
);
CREATE TABLE authors_books (
  authors_id BIGINT NOT NULL,
  books_id BIGINT NOT NULL,
  PRIMARY KEY (authors_id, books_id)
);